import numpy as np
from numpy import matlib as mat
import matplotlib.pyplot as plt
from numpy import linalg
from numpy import *
from chebychevTests import runtests_cheb
from chebychevTests import runtests_differentiate
from ODETests import runtests_ode
from scipy.optimize import fsolve
from scipy.integrate import odeint


def ChebMatrix(N):
	'''
	Creates a chebchev differential matrix of the size N,N. This can then
	be used to put into a root finder. In this we will use fsolve to solve
	a zero problem.
	'''
	t = np.arange(0,N+1)			
	x = (cos(np.pi*t/N)); 			
	k = np.append(2,np.ones((N-1,1)))
	c = np.array([(np.append(k,2)*(-1)**t)]).T
	X = np.tile(np.array(x),[N+1, 1]).T;
	dX = X-X.T; 
	D  = ((c*(1/c.T))/(dX+(identity(N+1))));  
	D  = D - np.diag(np.sum(D.T,0));               
	return D

#A function to differentiate	
def function(x,t,pars):
	#print(x[0,:])
	xddot = sin(pi*t) - 2*pars[1]*x[1] - pars[0]*x[0]
	#print(x)
	return np.array([x[1], xddot]).T

'''
#Tests to see if the chebychev is correct
runtests_cheb(ChebMatrix)
'''
def g(x,N,t,ODE,par):
	'''
	The zeroed problem equation
	'''
	x = x.reshape(N+1,2)
	res = ChebMatrix(N)@x - ODE(x,t,par)
	res[-1] = x[0]-x[-1]
	
	res = res.flatten()
	return 	res

def collocation(ODE, N, x0,pars):
	'''
	Solving the function between -1,1 and then returning the new x
	'''
	t = cos(np.pi*arange(0,N+1)/N)
	x = fsolve(g,x0,(N,t,ODE,pars))
		
	
	return x


#tests to check if the code works	
'''	
final = collocation(function, 20, zeros((21,2)),(0.2,0.05))
N = 10
chebychevpoints = (cos(np.pi*arange(0,N+1)/N))
D = ChebMatrix(N)
runtests_differentiate(D,chebychevpoints)

#x = collocation(function,30,zeros(31),[-2]))
#runtests_ode(collocation)
'''