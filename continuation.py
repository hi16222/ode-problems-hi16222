from Week6 import psuedo_arc
from Week5 import collocation
from Week4 import shooting
import numpy as np
from numpy import *
import matplotlib.pyplot as plt
from scipy.optimize import fsolve
from scipy.integrate import odeint

def augmented2(y, yhat, secant, ode, init_pars):
	'''
	Used in the pseudo arc length continuation for the shooting but 
	it doesn't work as intened at the moment
	'''
	z = shooting(ode,y[0:-1],(y[-1],init_pars))
	return [z[0],z[1],np.transpose(secant)@(y-yhat)]
	
'''
If you wish to input your own function then you must do it in the following format


The format for the equation in shooting is x' as x[1] and x as x[0] otherwise the code
will not run. For example the mass spring equation 

xdot = x[1]
xddot = sin(pi*t) - 2*pars[1]*x[1] - pars[0]*x[0]	

For the collocation method any x' or x in the defined function will need to be written as
x[:,0] for x or x[:,1] for x' otherwise the function will not run.

for example duffing

xdot = x[:,1]
xddot = pars[2]*sin(pi*t) - 2*pars[1]*x[:,1] - pars[0]*x[:,0] - pars[3]*x[:,0]**3

Lamda will not take a differential equation. The format must simply be a zero problem
'''	
	
def duffing(x,t,pars):
	'''
	The duffing equation
	x'' + 2Ex' + kx + B*x**3 = rsin(pi*t)
	returned as a system of first order equations
	x' and
	rsin(pi*t) -2*Ex' - kx - B*x**3
	where k is the variable parameter
	'''
	
	xddot = pars[2]*sin(pi*t) - 2*pars[1]*x[:,1] - pars[0]*x[:,0] - pars[3]*x[:,0]**3
	
	return np.array([x[:,1],xddot]).T
			

def mass_spring(x, t, pars):
	'''
	The mass spring equation
	x'' + 2Ex' + kx = sin(pi*t)
	returned as a system of 1st order equations
	x' and
	sin(pi*t) - 2Ex' - kx
	Where k is the variable paramerer
	'''
	
	xddot = sin(pi*t) - 2*pars[1]*x[1] - pars[0]*x[0]
	return np.array([x[1],xddot]).T
	
def cubic(x,pars):
	'''
	A simple cubic polynomial
	x**3 - x - c
	where c is a varyable parameter
	'''
	return x**3 - x - pars[0]

def main(ODE, init_state, vary_pars, init_pars, step_size, max_steps, 
	discretisation = 'shooting'):
    
	pars = np.zeros(len(init_pars) +1)
	
	pars[0] = vary_pars
	for niter in range (len(init_pars)):
		pars[niter+1] = init_pars[niter]
		
		
	if discretisation == 'shooting':
		'''
		Natural parameter continuation
		Increases the variable parameter by the stepsize for the maximum number of steps.
		at each value of c, the variable parameter, it uses shooting to find the root of the function
		
		=======
		'''
		
		results = np.zeros((max_steps,3)) #initialising results matrix
		x0 = shooting(ODE, init_state,pars) #corrected initial_state
		c0 = pars[0] 
		for niter in range(len(x0)):
			results[0,niter] = (x0[niter])
		results[0,-1] = c0
		
		c1 = np.append(c0,pars[1:]) #Merged the iterated parameter with the initial paramerers
		
		for i in range(max_steps):
			x1 = shooting(ODE,x0,(c1))
			for niter in range(len(x1)):
				results[i,niter] = (abs(x1[niter]))
			results[i,-1] = c1[0]
			#print(results)
			#results[i,:] = (abs(x1[0]),abs(x1[1]),c1[0])
			x0 = x1
			c1[0] = c1[0] +step_size
		print(results)
		plt.plot(results[:,2],results[:,0],'.-') #shows the plot
		plt.show()
		
		'''
		=======
		
		Pseudo arc length continuation
		
		It is still a work in progress for shooting
		
		
		=======
		'''
		'''
		xx = np.zeros((max_steps+2,3))
		amp = np.zeros(max_steps+2)
		
		def take_amplitude(ODE,x,period,pars):
			sol = odeint(ODE,x,[0,2,101],args = (pars,))
			maxSol = np.amax(sol)
			minSol = np.amin(sol)

			return maxSol - minSol
		
		x0 = shooting(ODE, init_state, pars)
		c1 = 0.2
		x1 = shooting(ODE,x0,(c1,pars[1]))
		
		xx[0,:] = ([x0[0],x0[1],pars[0]])
		xx[1,:] = ([x1[0],x0[1],c1])
		
		y0 = xx[0,:]
		y1 = xx[1,:]
		amp[0] = take_amplitude(ODE,y0[0:2],2,(y0[-1],pars[1]))
		amp[1] = take_amplitude(ODE,y1[0:2],2,(y1[-1],pars[1]))
		
		for i in range(max_steps):
			
			secant = y1 - y0
			y2hat = y1 + secant
			#c2 = np.transpose(secant)@(y2hat-y2hat)
			#y2 = shooting(ODE,y2hat[0:-1],(c2,pars[1]))
			#y2 = np.array([y2[0],y2[1],c2])
			y2 = fsolve(augmented2, y2hat,(y2hat, secant, ODE, pars[1]))
			xx[i+2,:] = y2
			
			amp[i+2] = take_amplitude(ODE,y2[0:2],2,(y2[-1],pars[1]))
			y0 = y1
			y1 = y2
		
		plt.plot(xx[:,2],amp,'.-')
		plt.show()
		=======
		'''
	elif discretisation == 'collocation':
		'''
		This performs natural parameter continuation using collocation to 
		find the roots of the differential equation at different values of c
		'''
	
		cheb_size = 20 # setting the size of the chebychev matrix
	
		
		x0 = collocation(ODE,cheb_size,np.zeros((cheb_size+1,2)),pars) #corrected initial state
		c1 = np.append(pars[0] + step_size,pars[1:])
		
		x1 = collocation(ODE,cheb_size,x0,c1)
		
		y0 = x0.reshape(cheb_size+1,2)
		y1 = x1.reshape(cheb_size+1,2)
		
		xx = np.array([max(y0[:,0]),max(y1[:,0])])
		xx = np.append(xx, np.zeros(max_steps))


		cc = np.array([pars[0],c1[0]])
		cc = np.append(cc, np.zeros(max_steps))   
		
		
		
		
		for i in range(max_steps):
			x2 = collocation(ODE,cheb_size,x1,(c1)).reshape(cheb_size+1,2)
			xx[i+2] = max(x2[:,0])
			cc[i+2] = c1[0]
			c1[0] =c1[0] + step_size
			x1 = x2;
		
		plt.plot(cc,xx,'.-')
		plt.show()
		
		
		#Pseudo arc length continuation 
		#Still a work n progress for collocation
		'''
		===
		x0 = collocation(ODE,20,np.zeros((21,2)),pars).reshape(21,2)
		c1 = pars[0] + step_size;
		
		x1 = collocation(ODE,20,x0,(c1,pars[1])).reshape(21,2)
		
		y0 = np.array([max(abs(x0[0])),max((x0[1])),pars[0]])
		y1 = np.array([max(abs(x1[0])),max((x1[1])),c1])
		
		xx = np.zeros((max_steps+2,2))
		xx[0,:] = (max(abs(x0[0])),max(abs(x0[1])))
		xx[1,:] = (max(abs(x1[0])),max(abs(x1[1])))
		
		
		cc = np.zeros((max_steps+2))
		cc[0:2] = (pars[0],c1) 
		
		for i in range(max_steps):
			secant = y1 - y0
			y2hat = np.array(y1 + secant)
			y2 = fsolve(augmented2, y2hat, args = (y2hat, secant,ODE,pars[1]))
			xx[i+2] = max(y2[:,0:-1])
			cc[i+2] = y2[-1]
			x1 = x2;
		===
		'''
	elif discretisation == 'lambda':
		psuedo_arc(ODE,init_state, pars, step_size, max_steps)
	
	else:
		return print('error')

#Initial x values to input into the code
init_shooting = [1, 1]
init_collocation = np.zeros((21,2))
init_lambda = -1


vary_pars = 0.1 #Initial value for the variable parameter
step_size = 0.1 #Stepsize for the variable parameter
max_steps = 200 #Number of steps for collocation and shooting
max_steps_lambda = 20 #Number of steps for the lambda and pseudo arc length
init_pars = [0.05,0.5, 1] #Initial parameters for the functions. These don't vary


if __name__	== '__main__':
	main(cubic,init_lambda, vary_pars, init_pars, step_size, max_steps_lambda, 
		discretisation ='lambda')
	main(duffing,init_collocation, vary_pars, init_pars, step_size, max_steps, 
		discretisation ='collocation')
	main(mass_spring,init_shooting, vary_pars, init_pars, step_size, max_steps, 
		discretisation ='shooting')