'''
import the necessary functions from other libraries
'''

import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
from scipy.optimize import fsolve

'''
Initialise parameters for the functions
'''


def f(y,t,r,w,E):
	'''
	function for the duffing equation and parameters
	'''
	u, x = y
	udot = x
	xdot = r*np.sin(w*t) - 2*E*x - u - u**3
	dydt = [udot,xdot]
	return dydt




def g(x, t, ODE, pars):
	'''
	This is the zero problem equation that we can pass into fsolve
	to find the corrected x value.
	'''
	h = odeint(ODE, x, t, args=(pars,))
	g = x - h[-1,:]
	return g


def shooting(ODE, init_state, pars):
	period = 2

	
	ts = np.linspace(0,100*period,1001)
	#sol = odeint(ODE, init_state, ts, args = (pars,))
	
	'''
	plt.plot(ts,sol[:,0])
	plt.xlabel('time')
	plt.ylabel('solution')
	plt.show()
	'''
	

	tsingle = np.linspace(0,period,501)
	#sol = odeint(ODE,y0 ,tsingle,args = (pars,))
	#y1 = [max(sol[0]),max(sol[1])]
	
	
	x, info, ier, mesg = fsolve(g, init_state,args = (tsingle,ODE, pars,), full_output=True)
	#print(x)
	#plt.plot(tsingle,sol[:,0])
	#plt.show()
	return  x
	'''
	xvals = []
	
	for i in range(0, 10):  # 10 random trials
		x0 = 4*np.random.rand(1, 2) - 2  # random numbers between [-2, 2]
		xnew, info, ier, mesg = fsolve(g, x0, args = period, full_output=True)
		if ier == 1:
			xvals.append(xnew)
	xvals = np.sort(xvals)
	print(xvals)
	'''

	



