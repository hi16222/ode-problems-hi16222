import numpy as np
from scipy.optimize import fsolve
import matplotlib.pyplot as plt
	
def augmented(y, yhat, secant, ode, init_pars):
	'''
	This is the arc length equation that will find the roots of the function
	and then at what the variable parameter is at that point. This is good as it will not miss multiple roots for the same parameter value
	'''

	z = ode(y[0:-1],(y[-1],init_pars))	
	return [z,np.transpose(secant)@(y-yhat)]
	
def psuedo_arc(ode,init_state, pars, step_size, max_steps):
	'''
	x0,x1 are corrected initial point
	pars[0],c1 are vary paramiters numbers 1 and 2
	xx is and array of x values
	cc is an array of variable parameters
	Then it calulated a next point using the secant method and this is used as the new y for the augmented function.
	'''
	x0,info,ier,mesg = fsolve(ode, init_state, args = (pars), full_output=True)
	c1 = pars[0] + step_size;
	x1 = fsolve(ode, x0, ([c1,pars[1]]))
	xx = np.array([x0,x1])
	xx = np.append(xx, np.zeros(max_steps))


	cc = np.array([pars[0],c1])
	cc = np.append(cc, np.zeros(max_steps))         
	y0 = np.array([x0[0], pars[0]]);
	y1 = np.array([x1[0], c1]);

	for i in range(max_steps):
		secant = np.array(y1 - y0)
		y2hat = np.array(y1 + secant)
		
		y2 = fsolve(augmented, y2hat,(y2hat, secant, ode, pars))
		xx[i+2] = y2[0:-1]
		cc[i+2] = y2[1]
		y0 = y1;
		y1 = y2;
	
	plt.plot(cc,xx,'.-')
	plt.show()


